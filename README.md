# ohos-svg

## 简介

ohos-svg是一个SVG图片的解析器和渲染器，可以解析SVG图片并渲染到页面上，还可以动态改变SVG的样式。

## 效果展示
SVG解析:

<img src="screenshot/example1.png"/>

多个图片解析:

<img src="screenshot/example2.png"/>

改变图片样式:

<img src="screenshot/example3.png"/>

动画效果解析SVG:

<img src="screenshot/example4.png"  />



动画效果解析SVG和图片:

<img src="screenshot/example5.png"/>


## 下载安装

```shell
npm install @ohos/ohos-svg --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

 ```
 import {SVGImageView} from '@ohos/ohos_svg/'
 
 build() {
   SVGImageView({ svgString: "SVG文件字符串"})
 }
 ```

## 接口说明
`let svgManager = SVGManager.getInstance()`
1. 解析SVG文件
   `svgManager.parseFile(fileName: string, onSuccess: Function, onFailed?: Function)`
2. 解析SVG字符串
   `svgManager.parseString(fileString: string, onSuccess: Function, onFailed?: Function)`

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- ohos-svg  
|     |---- entry  # 示例代码文件夹
|     |---- ohos_svg  # ohos_svg库文件夹
|           |---- index.ets  # 对外接口
            |---- components  # 组件代码目录
                  |---- SVGImageView.ets # 核心组件
                  |---- common # 工具库
                        |---- constants # 常量
                              |---- SVGXMLConstants.ets # SVG属性
                        |---- SVGManager # SVG解析器
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos-svg/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/ohos-svg/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/ohos-svg/blob/master/LICENSE) ，请自由地享受和参与开源。